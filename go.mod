module gitlab.com/slhamptonii/sandbox-users

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.9.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/slhamptonii/sheldonsandbox-core/v3 v3.4.1
	gopkg.in/yaml.v2 v2.4.0
)
