package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/crypt"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/model"
)

type PasswordChange struct {
	Id               int    `json:"id"`
	OriginalPassword string `json:"originalPassword"`
	NewPassword      string `json:"newPassword"`
}

//endpoint to create a new user
func CreateUser(w http.ResponseWriter, r *http.Request) {
	user, err := payloadToUser(r)
	if err != nil {
		log.Println("error maping request to user", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = r.Body.Close()
	if err != nil {
		log.Println("error handling request", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	user.Password = crypt.HashAndSaltDefault([]byte(user.Password))
	if user.Password == "" {
		log.Println("unable to protect password")
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	user.CreatedDateTime = time.Now().UTC().Format(time.UnixDate)
	user.UpdatedDateTime = time.Now().UTC().Format(time.UnixDate)
	user.Roles = []model.Role{model.PLAYER}

	roleIds, err := checkRoles(user.Roles)
	if err != nil {
		log.Println("could not create user", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = db.QueryRow(
		"INSERT INTO sandbox.user(username, password,  created_date_time, updated_date_time, meta) VALUES($1, $2, $3, $4, $5) RETURNING id",
		user.Username, user.Password, user.CreatedDateTime, user.UpdatedDateTime, user.Meta).Scan(&user.Id)

	if err != nil {
		//TODO: handle 'pq: duplicate key value violates unique constraint "u_username"' error as 400
		log.Println("could not create user", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	for _, roleId := range roleIds {
		_ = db.QueryRow("INSERT INTO sandbox.user_roles(user_id, role_id) VALUES ($1, $2)", user.Id, roleId)
	}

	//remove password from response
	user.Password = ""

	respondWithJSON(w, http.StatusCreated, user)
}

//endpoint to retrieve a single user by their id
func GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userId := vars["userId"]

	//convert userId parameter to an int
	id, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	var user model.User
	err = db.QueryRow("SELECT * FROM sandbox.user WHERE id=$1", id).Scan(
		&user.Id, &user.Username, &user.Password, &user.CreatedDateTime, &user.UpdatedDateTime, &user.Meta)
	if err != nil {
		log.Println("could not find user", id, err.Error())
		respondWithError(w, http.StatusNotFound, "not found")
		return
	}

	roles := []model.Role{}
	rows, err := db.Query("SELECT role.ROLE FROM sandbox.role WHERE role.id IN (SELECT user_roles.role_id FROM sandbox.user_roles WHERE user_id=$1)", id)
	if err != nil {
		log.Println("could not find user roles", id, err.Error())
		respondWithError(w, http.StatusNotFound, "not found")
		return
	}

	for rows.Next() {
		var role model.Role
		if err := rows.Scan(&role); err != nil {
			log.Println("unable to get user roles for user", id, err.Error())
			respondWithError(w, http.StatusNotFound, "not found")
			return
		}
		roles = append(roles, role)
	}

	user.Roles = roles
	user.Password = ""

	respondWithJSON(w, http.StatusOK, user)
}

//endpoint to retrieve multiple users
func GetUsers(w http.ResponseWriter, r *http.Request) {
	countMin, countMax := 1, 100
	userCount := 1
	c := r.URL.Query().Get("count")

	if c != "" {
		count, err := strconv.Atoi(c)
		if err != nil {
			log.Println("could not convert count to int", err.Error())
			respondWithError(w, http.StatusBadRequest, "invalid count")
			return
		}

		if count < countMin {
			log.Println("moving count from", count, "to", countMin)
			count = countMin
		} else if count > countMax {
			log.Println("moving count from", count, "to", countMax)
			count = countMax
		}

		userCount = count
	}

	users := make([]model.User, 0)
	rows, err := db.Query(
		"SELECT * FROM sandbox.user LIMIT $1", userCount)
	if err != nil {
		log.Println("error querying users", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	for rows.Next() {
		var u model.User
		if err := rows.Scan(&u.Id, &u.Username, &u.Password, &u.CreatedDateTime, &u.UpdatedDateTime, &u.Meta); err != nil {
			log.Println("error mapping users", err.Error())
			respondWithError(w, http.StatusInternalServerError, "internal service error")
			return
		}
		users = append(users, u)
	}

	err = rows.Close()
	if err != nil {
		log.Println("error closing rows", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	for _, user := range users {
		roles := []model.Role{}
		rows, err := db.Query("SELECT role.ROLE FROM sandbox.role WHERE role.id IN (SELECT user_roles.role_id FROM sandbox.user_roles WHERE user_id=$1)", user.Id)
		if err != nil {
			log.Println("could not find user roles", user.Id, err.Error())
			respondWithError(w, http.StatusNotFound, "not found")
			return
		}

		for rows.Next() {
			var role model.Role
			if err := rows.Scan(&role); err != nil {
				log.Println("unable to get user roles for user", user.Id, err.Error())
				respondWithError(w, http.StatusNotFound, "not found")
				return
			}
			roles = append(roles, role)
		}

		user.Roles = roles
	}

	respondWithJSON(w, http.StatusOK, users)
}

//endpoint to update a user
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	user, err := payloadToUser(r)
	if err != nil {
		log.Println("error maping request to user", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = r.Body.Close()
	if err != nil {
		log.Println("error handling request", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	vars := mux.Vars(r)
	userId := vars["userId"]

	//convert userId parameter to an int
	id, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	if user.Id != id {
		log.Println("invalid ids for update", user.Id, id)
		respondWithError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	user.UpdatedDateTime = time.Now().UTC().Format(time.UnixDate)

	_, err = db.Exec("UPDATE sandbox.user SET username = $1, updated_date_time = $2 WHERE id = $3",
		user.Username, user.UpdatedDateTime, user.Id)
	if err != nil {
		log.Println("unable to update user", user.Id, id)
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = db.QueryRow("SELECT * FROM sandbox.user WHERE id=$1", id).Scan(
		&user.Id, &user.Username, &user.Password, &user.CreatedDateTime, &user.UpdatedDateTime, &user.Meta)
	if err != nil {
		log.Println("could not find user", id, err.Error())
		respondWithError(w, http.StatusNotFound, "not found")
		return
	}

	roles := []model.Role{}
	rows, err := db.Query("SELECT role.ROLE FROM sandbox.role WHERE role.id IN (SELECT user_roles.role_id FROM sandbox.user_roles WHERE user_id=$1)", id)
	if err != nil {
		log.Println("could not find user roles", id, err.Error())
		respondWithError(w, http.StatusNotFound, "not found")
		return
	}

	for rows.Next() {
		var role model.Role
		if err := rows.Scan(&role); err != nil {
			log.Println("unable to get user roles for user", id, err.Error())
			respondWithError(w, http.StatusNotFound, "not found")
			return
		}
		roles = append(roles, role)
	}

	user.Roles = roles
	user.Password = ""

	respondWithJSON(w, http.StatusOK, user)
}

//endpoint to delete user
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userId := vars["userId"]

	//convert userId parameter to an int
	id, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	if id < 0 {
		respondWithError(w, http.StatusNotFound, "not found")
		return
	}

	_, err = db.Exec("DELETE FROM sandbox.user_roles WHERE user_id=$1", id)
	if err != nil {
		log.Println("error deleting user", id, err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}
	_, err = db.Exec("DELETE FROM sandbox.user WHERE id=$1", id)
	if err != nil {
		log.Println("error deleting user", id, err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	respondWithJSON(w, http.StatusNoContent, nil)
}

//endpoint to reset password
func ResetPassword(w http.ResponseWriter, r *http.Request) {
	passwordChange, err := payloadToPasswordChange(r)
	if err != nil {
		log.Println("error maping request to passwordChange", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = r.Body.Close()
	if err != nil {
		log.Println("error handling request", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	vars := mux.Vars(r)
	userId := vars["userId"]

	//convert userId parameter to an int
	id, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	if passwordChange.Id != id {
		log.Println("invalid ids for update", passwordChange.Id, id)
		respondWithError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	var originalUser model.User
	err = db.QueryRow("SELECT * FROM sandbox.user WHERE id=$1", id).Scan(
		&originalUser.Id, &originalUser.Username, &originalUser.Password, &originalUser.CreatedDateTime, &originalUser.UpdatedDateTime, &originalUser.Meta)
	if err != nil {
		log.Println("could not find user", id, err.Error())
		respondWithError(w, http.StatusNotFound, "not found")
		return
	}

	if !crypt.ComparePasswords(originalUser.Password, []byte(passwordChange.OriginalPassword)) {
		log.Println("bad password reset")
		respondWithError(w, http.StatusForbidden, "forbidden")
		return
	}

	newPassword := crypt.HashAndSaltDefault([]byte(passwordChange.NewPassword))
	if newPassword == "" {
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	user := model.User{Id: passwordChange.Id, Password: newPassword}

	_, err = db.Exec("UPDATE sandbox.user SET username = $1, updated_date_time = $2 WHERE id = $3",
		user.Username, user.UpdatedDateTime, user.Id)
	if err != nil {
		log.Println("unable to update user", user.Id, id)
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	respondWithJSON(w, http.StatusNoContent, nil)
}

//endpoint to "login" to application via email
func Login(w http.ResponseWriter, r *http.Request) {
	stranger, err := payloadToUser(r)
	if err != nil {
		log.Println("error maping request to user", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = r.Body.Close()
	if err != nil {
		log.Println("error handling request", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	var user model.User
	err = db.QueryRow("SELECT * FROM sandbox.user WHERE username=$1", stranger.Username).Scan(
		&user.Id, &user.Username, &user.Password, &user.CreatedDateTime, &user.UpdatedDateTime, &user.Meta)
	if err != nil {
		log.Println("could not find user", err.Error())
		respondWithError(w, http.StatusNotFound, "not found")
		return
	}

	if user.Password != "" {
		if !crypt.ComparePasswords(user.Password, []byte(stranger.Password)) {
			respondWithError(w, http.StatusForbidden, "forbidden")
			return
		}
	} else {
		log.Println("could not find user", err.Error())
		respondWithError(w, http.StatusNotFound, "not found")
		return
	}

	user.Password = ""
	respondWithJSON(w, http.StatusOK, user)
}

//endpoint to "logout" user
func Logout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("x-ss", "")
	respondWithJSON(w, http.StatusNoContent, nil)
}

func checkRoles(roles []model.Role) ([]int, error) {
	rows, err := db.Query("SELECT * FROM sandbox.role")
	if err != nil {
		log.Println("error verifying roles", err.Error())
		return []int{}, err
	}

	roleIdMap := map[int]model.Role{}
	userRoles := make([]int, 0)
	for rows.Next() {
		var role model.Role
		var id int
		if err := rows.Scan(&id, &role); err != nil {
			log.Println("error verifying roles", err.Error())
			return []int{}, err
		}
		roleIdMap[id] = role
	}

	for _, role := range roles {
		ok := false
		for id, roleName := range roleIdMap {
			if role == roleName {
				ok = true
				userRoles = append(userRoles, id)
				break
			}
		}

		if !ok {
			return []int{}, errors.New(fmt.Sprintf("invalid role %v", role))
		}
	}

	return userRoles, nil
}

//Converts HTTP request body into a model.User struct
func payloadToUser(r *http.Request) (model.User, error) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Println("error decoding user", err.Error())
	}
	return user, err
}

//Converts HTTP request body into a PasswordChange struct
func payloadToPasswordChange(r *http.Request) (PasswordChange, error) {
	var passwordChange PasswordChange
	err := json.NewDecoder(r.Body).Decode(&passwordChange)
	if err != nil {
		log.Println("error decoding user", err.Error())
	}
	return passwordChange, err
}

//Clears signed string token and sets HTTP Response body to map of error messages
func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

//Sets status, body, and default headers to HTTP Response
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}
