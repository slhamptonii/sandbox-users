# builder image
FROM golang:alpine AS builder

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

WORKDIR /build

COPY go.mod go.sum handlers.go main.go ./

# Download all dependancies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

RUN go build -o main .

# final image
FROM alpine
LABEL maintainer="Sheldon L Hampton II <tharivolnailo@protonmail.com>"

WORKDIR /app

# using absolute path of artifact from build stage
COPY --from=builder /build/main ./main

COPY env/ ./env/

HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl --HEAD http://127.0.0.1/health || exit 1

# Run the executable
CMD ./main
