package main

import (
	"context"
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"gopkg.in/yaml.v2"
)

var db *sql.DB

func main() {
	log.Println("Hello, World!")

	path := fmt.Sprintf("./env/%s.yml", os.Getenv("ENV"))
	envPath, _ := filepath.Abs(path)

	data, err := ioutil.ReadFile(envPath)
	if err != nil {
		log.Fatalf("could not read env config file %v", err)
	}

	var config map[string]string
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		log.Fatalf("could not unmarshall yaml file %v", err)
	}

	//connect to database
	connectionString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		config["host"], config["port"], config["username"],
		config["password"], config["database"], config["ssl"])

	db, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal("unable to connect to database", err.Error())
	}

	//test connection
	if err := db.Ping(); err != nil {
		log.Fatal("could not ping database ", err.Error())
	}

	log.Println("successfully connected to database")

	router := mux.NewRouter()
	router.Methods("HEAD").Path("/health").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("healthy")
		w.WriteHeader(http.StatusOK)
	})

	router.Methods("POST").Path("/login").HandlerFunc(Login)
	router.Methods("POST").Path("/logout").HandlerFunc(Logout)

	router.Methods("POST").Path("/users").HandlerFunc(CreateUser)
	router.Methods("GET").Path("/users").HandlerFunc(GetUsers)
	router.Methods("GET").Path("/users/{userId:[0-9]+}").HandlerFunc(GetUser)
	router.Methods("PATCH").Path("/users/{userId:[0-9]+}").HandlerFunc(UpdateUser)
	router.Methods("PATCH").Path("/users/{userId:[0-9]+}/reset").HandlerFunc(ResetPassword)
	router.Methods("DELETE").Path("/users/{userId:[0-9]+}").HandlerFunc(DeleteUser)

	srv := &http.Server{
		Handler:      router,
		Addr:         ":80",
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		log.Println("Starting Server")
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal("application closing ", err.Error())
		}
	}()

	// Graceful Shutdown
	waitForShutdown(srv)
}

func waitForShutdown(srv *http.Server) {
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// Block until we receive our signal.
	<-interruptChan

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	srv.Shutdown(ctx)

	log.Println("Goodbye, cruel World!")
	os.Exit(0)
}
